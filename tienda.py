import sys
articulos={}
def anadir():

    productos = sys.argv[1::2]
    precios = sys.argv[2::2]

    for i in range(len(productos)):

        articulos[productos[i]] = precios[i]
def mostrar():
    print("articulos de la tienda:")
    for producto, precio in articulos.items():
        print(producto,precio)
def pedir_articulos():
    pedir = str()
    seguir=True
    while seguir:
        pedir = input("que articulo deseas: ")
        if pedir=="":
            print("introduce un producto de la lista")
        elif pedir not in articulos.keys():
            print("pon algo de la lista")
        else:
            break
    return pedir

def pedir_cantidad():
    try:
        cantidad=float(input("cuantos elemntos quires: "))
        return cantidad
    except ValueError:
        return pedir_cantidad()

def main():
    anadir()
    mostrar()
    articulo = pedir_articulos()
    cantidad=pedir_cantidad()
    total=float(articulos[articulo])*cantidad
    print(total)
if __name__ == "__main__":

    main()